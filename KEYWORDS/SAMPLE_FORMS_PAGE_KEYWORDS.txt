*** Settings ***
Documentation  This resource file that contains Deposit amount keywords
Resource  ../COMMON/INCLUDE.txt

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*** Keywords ***
Fill the Information in Sample Forms Page
	[Arguments]  ${INPUT_DATA}
	Select Checkbox  ${CUSTOMER_SERVICE_CHK}
	Checkbox Should Be Selected  ${CUSTOMER_SERVICE_CHK}
	Input Text  ${SUBJECT_ID}  ${INPUT_DATA['SUBJECT_VALUE']}
	Textfield Value Should Be  ${SUBJECT_ID}  ${INPUT_DATA['SUBJECT_VALUE']}
	Select From List  ${DROPDOWN_BOX_DROPDOWN}  ${INPUT_DATA['DROPDOWN_VALUE']}
	List Selection Should Be  ${DROPDOWN_BOX_DROPDOWN}  ${INPUT_DATA['DROPDOWN_VALUE']}
	Select Checkbox  ${FIFTH_OPTION_RADIO}
	Checkbox Should Be Selected  ${FIFTH_OPTION_RADIO}
	 